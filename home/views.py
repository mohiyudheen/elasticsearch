from django.shortcuts import render

# Create your views here.
from django.http import JsonResponse
import requests
import json
from home.models import *

from .documents import *
from .serializers import *

from django_elasticsearch_dsl_drf.filter_backends import (
    FilteringFilterBackend,
    CompoundSearchFilterBackend
)
from django_elasticsearch_dsl_drf.viewsets import DocumentViewSet
from django_elasticsearch_dsl_drf.filter_backends import (
    FilteringFilterBackend,
    OrderingFilterBackend,
)

from .documents import NewsDocument
from elasticsearch import Elasticsearch
from elasticsearch.helpers import bulk

from os.path import abspath, join, dirname, exists
import urllib3
from elasticsearch import Elasticsearch
from elasticsearch.helpers import streaming_bulk
    

def Queryindex(request): 
    data=[]
    a=NewsDocument.search().query("match",content="second")
    for i in a:
        data.append([i.title,i.content])
    #print("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx")
    #b=NewsDocument.search().filter("term",title="this")
    #for i in b:
        #data.append([i.title,i.content])
    return JsonResponse({'data' : data})



def gendata():
    mywords = [{'title':'bulk title1','content':'bulk title content1'},
               {'title':'bulk title2','content':'bulk title content2'}
               ]
    for word in mywords:
        yield {
            "_index": "elastic_demo",
            "title": word['title'],
            "content":word['content']
        }   
   
   
def create_index(client):
    """Creates an index in Elasticsearch if one isn't already there."""
    client.indices.create(
        index="elastic_demo",
        body={
            "settings": {"number_of_shards": 1},
            "mappings": {
                "properties": {
                    "title": {"type": "keyword"},
                    "content": {"type": "keyword"}
                }
            },
        },
        ignore=400,
    )
    
def index(request):
    client = Elasticsearch()
    print("Creating an index...")
    create_index(client)

    print("Indexing documents...")
    for ok, action in streaming_bulk(client=client, index="elastic_demo", actions=gendata(),refresh='wait_for'):
        print("Indexed")
    
    #ElasticDemo(title='python',content='elastic search content').save()
    return JsonResponse({'status' : 200})




class PublisherDocumentView(DocumentViewSet):
    document = NewsDocument
    serializer_class = NewsDocumentSerializer
    fielddata=True
    filter_backends = [
        FilteringFilterBackend,
        OrderingFilterBackend,
        CompoundSearchFilterBackend,
    ]
   
    search_fields = (
        'title',
        'content',
    )
    multi_match_search_fields = (
       'title',
        'content',
    )
    filter_fields = {
       'title' : 'title',
        'content' : 'content',
    }
    ordering_fields = {
        'id': None,
    }
    ordering = ( 'id'  ,)
        
  

